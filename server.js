var express = require('express');
//var router = express.Router();

const app = express();
const path = require('path');
const fs = require('fs');

//here is the magic
var publicDir = require('path').join(__dirname,'/static');
var webContent = require('path').join(__dirname,'/build');
app.use(express.static(publicDir));
app.use(express.static(webContent));
//app.use(cors(corsOptions));


app.get('/index',(req,res)=>{
  console.log(__dirname + '/build/index.html');
  res.sendFile(require('path').join(__dirname + '/build/index.html'));
});

app.get('/mock', function(req, res) {
    console.log(req.query);
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
	  res.write(fs.readFileSync(path.resolve(process.cwd(),`./mockServices/${req.query.serviceName}.json`),'utf8'));
	  res.end();

  });
app.listen(8085, () => console.log(`Express server running on port 8000`));
